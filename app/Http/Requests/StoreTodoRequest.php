<?php

namespace App\Http\Requests;

use App\TodoModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTodoRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name'        => ['required','string','max:'.TodoModel::MAX_NAME_LENGTH],
            'description' => ['required','string','max:'.TodoModel::MAX_DESCRIPTION_LENGTH],
            'status'      => ['sometimes','string',Rule::in(array_values(TodoModel::STATUSES))],
        ];
    }
    
}
