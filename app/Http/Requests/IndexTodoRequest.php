<?php

namespace App\Http\Requests;

use App\TodoModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IndexTodoRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'page'          => ['numeric','min:1'],
            'count'         => ['numeric','min:1','max:50'],
            'status'        => ['sometimes','string',Rule::in(TodoModel::STATUSES)],
            'name'          => ['sometimes','string','max:50'],
            'sortBy'        => ['sometimes',Rule::in('created_at','updated_at')],
            'sortDirection' => ['sometimes',Rule::in('asc','desc')],
        ];
    }
    
}
