<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SingleTodo
 *
 * @package App\Http\Resources
 * @mixin \App\TodoModel
 */
class SingleTodo extends JsonResource
{
    
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'status'      => $this->status,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'description' => $this->description,
        ];
    }
    
}
