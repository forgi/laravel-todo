<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexTodoRequest;
use App\Http\Requests\StoreTodoRequest;
use App\Http\Requests\UpdateTodoRequest;
use App\Http\Resources\SingleTodo;
use App\TodoModel;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    
    /**
     * @param \App\Http\Requests\IndexTodoRequest $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(IndexTodoRequest $request)
    {
        
        $query = TodoModel::query();
        
        $query->orderBy($request->get('sortBy','created_at'),$request->get('sortDirection','desc'));
        
        return SingleTodo::collection($query->paginate($request->get('count',25)));
    }
    
    /**
     * @param $todoId
     *
     * @return \App\Http\Resources\SingleTodo
     */
    public function show($todoId)
    {
        
        $todo = TodoModel::query()->findOrFail($todoId);
        
        return new SingleTodo($todo);
    }
    
    /**
     * @param \App\Http\Requests\StoreTodoRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTodoRequest $request)
    {
        
        $todo = TodoModel::query()->create(
            array_merge(['status' => TodoModel::STATUSES['new']],$request->validated())
        );
        
        return response()->json(['id' => $todo->id],201);
    }
    
    /**
     * @param \App\Http\Requests\UpdateTodoRequest $request
     * @param int                                  $todoId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(UpdateTodoRequest $request,int $todoId)
    {
        
        /* @var TodoModel $todo */
        $todo = TodoModel::query()->findOrFail($todoId);
        $todo->update($request->validated());
        
        return response()->json(null,204);
    }
    
    /**
     * @param int $todoId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $todoId)
    {
        
        if($todo = TodoModel::query()->find($todoId)){
            $todo->delete();
        }
        
        return response()->json(null,204);
    }
    
}
