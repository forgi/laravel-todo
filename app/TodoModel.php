<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use InvalidArgumentException;

/**
 * Class Todo
 *
 * @property int                        $id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string                     $name
 * @property string                     $description
 * @property string                     $status
 *
 * @package App
 */
class TodoModel extends Model
{
    
    const STATUSES               = [
        'new'         => 'new',
        'in_progress' => 'in_progress',
        'done'        => 'done',
    ];
    
    const MAX_NAME_LENGTH        = 50;
    
    const MAX_DESCRIPTION_LENGTH = 1000;
    
    protected $table    = 'todos';
    
    protected $fillable = [
        'name',
        'description',
        'status',
    ];
    
    /**
     * @param $newValue
     */
    public function setStatusAttribute($newValue)
    {
        
        $newValue = trim(Str::snake($newValue));
        
        if(!in_array($newValue,self::STATUSES)){
            throw new InvalidArgumentException("Given status [$newValue] must be one of the followings: ".implode(',',self::STATUSES));
        }
        
        $this->attributes['status'] = $newValue;
    }
    
}
