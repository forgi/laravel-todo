<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TodoModel;
use Faker\Generator as Faker;

$factory->define(TodoModel::class,function(Faker $faker){
    
    return [
        'name'        => $faker->realText(TodoModel::MAX_NAME_LENGTH),
        'description' => $faker->realText(TodoModel::MAX_DESCRIPTION_LENGTH),
        'status'      => $faker->randomElement(TodoModel::STATUSES),
    ];
});

$factory->state(TodoModel::class,'status.new',function(Faker $faker){
    
    return [
        'status' => TodoModel::STATUSES['new'],
    ];
});

$factory->state(TodoModel::class,'status.in_progress',function(Faker $faker){
    
    return [
        'status' => TodoModel::STATUSES['in_progress'],
    ];
});
$factory->state(TodoModel::class,'status.done',function(Faker $faker){
    
    return [
        'status' => TodoModel::STATUSES['done'],
    ];
});
