<?php

use App\TodoModel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('todos',function(Blueprint $table){
            
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name',50);
            $table->mediumText('description');
            $table->enum('status',TodoModel::STATUSES);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('todos');
    }
    
}
