<?php

namespace Tests\Feature\TodoController;

use App\TodoModel;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexTest extends TestCase
{
    
    use RefreshDatabase;
    
    /**
     * @param \Illuminate\Foundation\Testing\TestResponse $response
     */
    protected function assertStructure(TestResponse $response)
    {
        
        $response->assertJsonStructure([
                                           'data' => [
                                               '*' => [
                                                   'id',
                                                   'created_at',
                                                   'updated_at',
                                                   'name',
                                                   'description',
                                                   'status',
                                               ],
                                           ],
                                       ]);
    }
    
    /** @test */
    public function can_list_todos()
    {
        
        factory(TodoModel::class)->times(5)->create();
        
        $response = $this->getJson(route('todo.index'));
        
        $response->assertStatus(200);
        $response->assertJsonCount(5,'data');
        $this->assertStructure($response);
    }
    
    /** @test */
    public function can_return_todos_paginated()
    {
        
        factory(TodoModel::class)->times(10)->create();
        
        $response = $this->getJson(route('todo.index',[
            'count' => 5,
            'page'  => 1,
        ]));
        
        $response->assertStatus(200);
        $response->assertJsonCount(5,'data');
        $this->assertStructure($response);
    }
    
    /** @test */
    public function default_per_page_count_is_25_and_page_is_the_first()
    {
        
        factory(TodoModel::class)->times(26)->create();
        
        $response = $this->getJson(route('todo.index'));
        
        $response->assertStatus(200);
        $response->assertJsonCount(25,'data');
        $this->assertStructure($response);
    }
    
    /** @test */
    public function can_sort_list_by_ascending_update_datetime()
    {
        
        /* @var TodoModel $todo1 */
        $todo1 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,01)]);
        /* @var TodoModel $todo2 */
        $todo2 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,02)]);
        /* @var TodoModel $todo3 */
        $todo3 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,03)]);
        
        $response = $this->getJson(route('todo.index',[
            'sortBy'        => 'updated_at',
            'sortDirection' => 'asc',
        ]));
        $response->assertStatus(200);
        
        $this->assertTrue($todo1->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.0.updated_at'))));
        $this->assertTrue($todo2->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.1.updated_at'))));
        $this->assertTrue($todo3->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.2.updated_at'))));
    }
    
    /** @test */
    public function can_sort_list_by_descending_update_datetime()
    {
        
        /* @var TodoModel $todo1 */
        $todo1 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,01)]);
        /* @var TodoModel $todo2 */
        $todo2 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,02)]);
        /* @var TodoModel $todo3 */
        $todo3 = factory(TodoModel::class)->create(['updated_at' => Carbon::create(2000,01,03)]);
        
        $response = $this->getJson(route('todo.index',[
            'sortBy'        => 'updated_at',
            'sortDirection' => 'desc',
        ]));
        $response->assertStatus(200);
        
        $this->assertTrue($todo1->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.2.updated_at'))));
        $this->assertTrue($todo2->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.1.updated_at'))));
        $this->assertTrue($todo3->updated_at->isSameDay(Carbon::createFromTimeString($response->json('data.0.updated_at'))));
        
    }
    
    /** @test */
    public function can_sort_list_by_ascending_creation_datetime()
    {
        
        /* @var TodoModel $todo1 */
        $todo1 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,01)]);
        /* @var TodoModel $todo2 */
        $todo2 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,02)]);
        /* @var TodoModel $todo3 */
        $todo3 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,03)]);
        
        $response = $this->getJson(route('todo.index',[
            'sortBy'        => 'created_at',
            'sortDirection' => 'asc',
        ]));
        $response->assertStatus(200);
        
        $this->assertTrue($todo1->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.0.created_at'))));
        $this->assertTrue($todo2->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.1.created_at'))));
        $this->assertTrue($todo3->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.2.created_at'))));
    }
    
    /** @test */
    public function can_sort_list_by_descending_creation_datetime()
    {
        
        /* @var TodoModel $todo1 */
        $todo1 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,01)]);
        /* @var TodoModel $todo2 */
        $todo2 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,02)]);
        /* @var TodoModel $todo3 */
        $todo3 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,03)]);
        
        $response = $this->getJson(route('todo.index',[
            'sortBy'        => 'created_at',
            'sortDirection' => 'desc',
        ]));
        $response->assertStatus(200);
        
        $this->assertTrue($todo1->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.2.created_at'))));
        $this->assertTrue($todo2->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.1.created_at'))));
        $this->assertTrue($todo3->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.0.created_at'))));
    }
    
    /** @test */
    public function list_is_sorted_by_descending_creation_datetime_by_default()
    {
        
        /* @var TodoModel $todo1 */
        $todo1 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,01)]);
        /* @var TodoModel $todo2 */
        $todo2 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,02)]);
        /* @var TodoModel $todo3 */
        $todo3 = factory(TodoModel::class)->create(['created_at' => Carbon::create(2000,01,03)]);
        
        $response = $this->getJson(route('todo.index'));
        $response->assertStatus(200);
        
        $this->assertTrue($todo1->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.2.created_at'))));
        $this->assertTrue($todo2->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.1.created_at'))));
        $this->assertTrue($todo3->created_at->isSameDay(Carbon::createFromTimeString($response->json('data.0.created_at'))));
    }
    
}
