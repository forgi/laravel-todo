<?php

namespace Tests\Feature\TodoController;

use App\TodoModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTest extends TestCase
{
    
    use RefreshDatabase;
    
    /** @test */
    public function can_show_existing_todo()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $response = $this->getJson(route('todo.show',$todo->id));
        
        $response->assertStatus(200);
        $response->assertJsonStructure([
                                           'data' => [
                                               'id',
                                               'name',
                                               'status',
                                               'created_at',
                                               'updated_at',
                                               'description',
                                           ],
                                       ]);
        $response->assertJsonFragment([
                                          'id'          => $todo->id,
                                          'name'        => $todo->name,
                                          'status'      => $todo->status,
                                          'description' => $todo->description,
                                      ]);
        
    }
    
    /** @test */
    public function fails_with_404_when_todo_does_not_exist()
    {
        
        $response = $this->getJson(route('todo.show',1));
        $response->assertStatus(404);
    }
    
}
