<?php

namespace Tests\Feature\TodoController;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    
    use WithFaker, RefreshDatabase;
    
    /** @test */
    public function when_no_status_given_it_creates_todo_with_new_status()
    {
        
        $payload = [
            'name'        => $name = $this->faker->realText(50),
            'description' => $description = $this->faker->realText(1000),
        ];
        
        $response = $this->postJson(route('todo.store'),$payload);
        
        $response->assertStatus(201);
        $response->assertJsonStructure(['id']);
        $generatedId = $response->json('id');
        
        $this->assertDatabaseHas('todos',[
            'id'          => $generatedId,
            'name'        => $name,
            'description' => $description,
            'status'      => 'new',
        ]);
    }
    
    /** @test */
    public function can_store_new_todo_with_status_new()
    {
        
        $payload = [
            'name'        => $name = $this->faker->realText(50),
            'description' => $description = $this->faker->realText(1000),
            'status'      => 'new',
        ];
        
        $response = $this->postJson(route('todo.store'),$payload);
        
        $response->assertStatus(201);
        $response->assertJsonStructure(['id']);
        $generatedId = $response->json('id');
        
        $this->assertDatabaseHas('todos',[
            'id'          => $generatedId,
            'name'        => $name,
            'description' => $description,
            'status'      => 'new',
        ]);
    }
    
    /** @test */
    public function can_store_new_todo_with_status_in_progress()
    {
        
        $payload = [
            'name'        => $name = $this->faker->realText(50),
            'description' => $description = $this->faker->realText(1000),
            'status'      => 'in_progress',
        ];
        
        $response = $this->post(route('todo.store'),$payload);
        
        $response->assertStatus(201);
        $response->assertJsonStructure(['id']);
        $generatedId = $response->json('id');
        
        $this->assertDatabaseHas('todos',[
            'id'          => $generatedId,
            'name'        => $name,
            'description' => $description,
            'status'      => 'in_progress',
        ]);
    }
    
    /** @test */
    public function can_store_new_todo_with_status_done()
    {
        
        $payload = [
            'name'        => $name = $this->faker->realText(50),
            'description' => $description = $this->faker->realText(1000),
            'status'      => 'done',
        ];
        
        $response = $this->postJson(route('todo.store'),$payload);
        
        $response->assertStatus(201);
        $response->assertJsonStructure(['id']);
        $generatedId = $response->json('id');
        
        $this->assertDatabaseHas('todos',[
            'id'          => $generatedId,
            'name'        => $name,
            'description' => $description,
            'status'      => 'done',
        ]);
    }
    
}
