<?php

namespace Tests\Feature\TodoController;

use App\TodoModel;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    
    use RefreshDatabase,WithFaker;
    
    const POSSIBLE_STATUSES = ['new','in_progress','done'];
    
    /**
     * @param       $id
     * @param array $payload
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function updateTodo($id,array $payload = [])
    {
        
        return $this->putJson(route('todo.update',$id),$payload);
    }
    
    /** @test */
    public function cannot_update_todo_which_does_not_exist()
    {
        
        $payload  = [
            'name'        => $name = $this->faker->realText(50),
            'description' => $description = $this->faker->realText(1000),
        ];
        $response = $this->updateTodo(1,$payload);
        $response->assertNotFound();
    }
    
    /** @test */
    public function can_update_todo_name()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $payload  = ['name' => $name = Str::random(50)];
        $response = $this->updateTodo($todo->id,$payload);
        $response->assertStatus(204);
        $this->assertTrue($response->isEmpty());
        
        $this->assertDatabaseHas('todos',[
            'id'   => $todo->id,
            'name' => $name,
        ]);
        
        $this->assertDatabaseMissing('todos',[
            'id'   => $todo->id,
            'name' => $todo->name,
        ]);
    }
    
    /** @test */
    public function can_update_todo_description()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $payload  = ['description' => $description = Str::random(1000)];
        $response = $this->updateTodo($todo->id,$payload);
        $response->assertStatus(204);
        $this->assertTrue($response->isEmpty());
        
        $this->assertDatabaseHas('todos',[
            'id'          => $todo->id,
            'description' => $description,
        ]);
        
        $this->assertDatabaseMissing('todos',[
            'id'          => $todo->id,
            'description' => $todo->name,
        ]);
    }
    
    /**
     * Make sure that if the same status applied on the todo
     * it won't raise an error.
     *
     * @test
     */
    public function can_update_todo_status_to_the_same()
    {
        
        foreach(self::POSSIBLE_STATUSES as $status){
            
            $todo = factory(TodoModel::class)->state('status.'.$status)->create();
            
            $response = $this->updateTodo($todo->id,[
                'status' => $status,
            ]);
            
            $response->assertStatus(204);
            $this->assertTrue($response->isEmpty());
            
            $this->assertDatabaseHas('todos',[
                'id'     => $todo->id,
                'status' => $status,
            ]);
        }
    }
    
    /** @test */
    public function can_update_todo_status_from_any_to_any_other()
    {
        
        // Iterate over the possible statuses.
        foreach(self::POSSIBLE_STATUSES as $status){
            
            // Pick up the statuses not used in the current iteration.
            $newStatuses = array_diff(self::POSSIBLE_STATUSES,[$status]);
            
            // Iterate over all other kind of statuses.
            foreach($newStatuses as $newStatus){
                
                // Scaffold an instance with the outer iteration status.
                $todo = factory(TodoModel::class)->state('status.'.$status)->create();
                
                // Apply the new status.
                $response = $this->updateTodo($todo->id,[
                    'status' => $newStatus,
                ]);
                
                $response->assertStatus(204);
                $this->assertTrue($response->isEmpty());
                
                // Assert the new status is persisted.
                $this->assertDatabaseHas('todos',[
                    'id'     => $todo->id,
                    'status' => $newStatus,
                ]);
            }
        }
        
    }
    
    /** @test */
    public function cannot_update_todo_when_name_is_longer_than_50()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $response = $this->updateTodo($todo->id,[
            'name' => $name = Str::random(51),
        ]);
        
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');
    }
    
    /** @test */
    public function cannot_update_todo_when_description_is_longer_than_1000()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $response = $this->updateTodo($todo->id,[
            'description' => $description = Str::random(1001),
        ]);
        
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('description');
    }
    
}
