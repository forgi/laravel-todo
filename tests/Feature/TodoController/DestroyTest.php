<?php

namespace Tests\Feature\TodoController;

use App\TodoModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DestroyTest extends TestCase
{
    
    use RefreshDatabase;
    
    /**
     * @param $id
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function destroyTodo($id)
    {
        
        return $this->deleteJson(route('todo.destroy',$id));
    }
    
    /** @test */
    public function can_destroy_todo()
    {
        
        $todo = factory(TodoModel::class)->create();
        
        $response = $this->destroyTodo($todo->id);
        
        $response->assertStatus(204);
        $this->assertTrue($response->isEmpty());
        
        $this->assertDatabaseMissing('todos',[
            'id' => $todo->id,
        ]);
    }
    
    /** @test */
    public function destroying_todo_is_idempotent()
    {
        
        $response = $this->destroyTodo(1);
        
        $response->assertStatus(204);
        $this->assertTrue($response->isEmpty());
    }
    
}
